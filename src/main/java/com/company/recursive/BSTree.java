package com.company.recursive;


import com.company.interfaces.ITree;
import com.company.model.Node;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class BSTree implements ITree {
    private int size = 0;
    private Node root;

    public BSTree() {

    }

    @Override
    public void init(int[] arr) {
        clear();
        for (int i = 0; i < arr.length; i++) {
            add(arr[i]);
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        int[] array = sortArrayBubble(toArray());
        for (int i = 0; i < array.length; i++) {
            stringArray += array[i];
            if (i != array.length - 1) {
                stringArray += ", ";
            }
        }
        stringArray += "]";
        System.out.println(stringArray);
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int[] toArray() {
        ArrayList<Node> arrayList = new ArrayList<>();

        if (root == null) {
            return new int[0];
        }

        fillArray(root, arrayList);

        int[] array = new int[arrayList.size()];

        Iterator<Node> iterator = arrayList.iterator();

        for (int i = 0; i < array.length; i++) {
            Node current = iterator.next();
            array[i] = current.item;
        }

        return array;
    }

    private void fillArray(Node node, ArrayList<Node> arrayList) {

        if (node.left != null) {
            fillArray(node.left, arrayList);
        }

        arrayList.add(node);

        if (node.right != null) {
            fillArray(node.right, arrayList);
        }
    }

    @Override
    public void add(int val) {
        Node newNode = new Node(val);
        if (root == null) {
            root = newNode;
            size++;
        } else {
            add(newNode, root);
        }
    }

    private void add(Node toAdd, Node current) {
        if (current.item < toAdd.item) {
            if (current.right == null) {
                current.right = toAdd;
                size++;
            } else {
                add(toAdd, current.right);
            }
        } else if (current.item > toAdd.item) {
            if (current.left == null) {
                current.left = toAdd;
                size++;
            } else {
                add(toAdd, current.left);
            }
        }
    }

    @Override
    public boolean del(int val) {
        Node deletedNode = deleteNode(root, val);
        if (deletedNode == null) {
            return false;
        } else {
            size--;
            return true;
        }
    }

    private Node deleteNode(Node current, int val) {
        if (current == null)
            return current;

        if (current.item > val) {
            current.left = deleteNode(current.left, val);
            return current;
        } else if (current.item < val) {
            current.right = deleteNode(current.right, val);
            return current;
        }

        if (current.left == null) {
            return current.right;
        } else if (current.right == null) {
            return current.left;
        } else {
            Node succParent = current;
            Node succ = current.right;
            while (succ.left != null) {
                succParent = succ;
                succ = succ.left;
            }

            if (succParent != current)
                succParent.left = succ.right;
            else
                succParent.right = succ.right;
            current.item = succ.item;

            return current;
        }
    }

    private Node receiveHeir(Node node) {
       Node parentNode = node;
       Node heirNode = node;
       Node currentNode = node.right;
        while (currentNode != null) {
            parentNode = heirNode;
            heirNode = currentNode;
            currentNode = currentNode.left;
        }
        if (heirNode != node.right) {
            parentNode.left = heirNode.right;
            heirNode.right = node.right;
        }
        return heirNode;
    }

    @Override
    public int getWidth() {
        if (root == null) {
            return 0;
        }

        int levels = getHeight();
        int[] levelsWidth = new int[levels];
        countLevel(1, root, levelsWidth);
        int maxSize = levelsWidth[0];

        for (int i = 1; i < levelsWidth.length; i++) {
            if (levelsWidth[i] > maxSize) {
                maxSize = levelsWidth[i];
            }
        }

        return maxSize;
    }

    public void countLevel(int level, Node node, int[] levelsWidth) {

        if (node.left != null) {
            countLevel(level + 1, node.left, levelsWidth);
        }

        if (node.right != null) {
            countLevel(level + 1, node.right, levelsWidth);
        }

        levelsWidth[level - 1] += 1;
    }

    @Override
    public int getHeight() {
        return getHeight(root);
    }

    private int getHeight(Node node) {
        if (node == null) {
            return 0;
        }
        return 1 + Math.max(getHeight(node.left), getHeight(node.right));
    }

    @Override
    public int nodes() {
        int nodesNotLeaves = size - leaves();
        return nodesNotLeaves;
    }

    @Override
    public int leaves() {
        if (root == null) {
            return 0;
        }

        return leaves(root);
    }

    private int leaves(Node current) {
        if (current.left == null && current.right == null) {
            return 1;
        }

        int sumHeirs = 0;

        if (current.left != null) {
            sumHeirs += leaves(current.left);
        }

        if (current.right != null) {
            sumHeirs += leaves(current.right);
        }

        return sumHeirs;
    }

    @Override
    public void reverse() {
        if (root != null) {
            reverse(root);
        }
    }

    private void reverse(Node node) {
        if (node.left != null) {
            reverse(node.left);
        }

        if (node.right != null) {
            reverse(node.right);
        }

        if (node.left != null || node.right != null) {
            Node temp = node.left;
            node.left = node.right;
            node.right = temp;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BSTree bsTree = (BSTree) o;
        return size == bsTree.size && Objects.equals(root, bsTree.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, root);
    }

    private int[] sortArrayBubble(int[] array) {
        boolean notSorted = true;
        while (notSorted) {
            notSorted = false;
            for (int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    int x = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = x;
                    notSorted = true;
                }
            }
        }
        return array;
    }
}
