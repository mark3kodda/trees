package com.company;

import com.company.recursive.BSTree;

public class Main {
    public static void main(String[] args) {
        BSTree bt = new BSTree();

        int[] myArr = new int []{6, 4, 1, 12, 63, 8, 2};

        bt.init(myArr);
        bt.print();
    }
}
