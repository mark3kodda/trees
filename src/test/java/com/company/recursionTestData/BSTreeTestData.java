package com.company.recursionTestData;

import com.company.model.Node;
import com.company.recursive.BSTree;
import java.lang.reflect.Field;

public class BSTreeTestData {
    public static final BSTree ORIGINAL_TREE_TEST_1 = new BSTree();
    public static final BSTree ORIGINAL_TREE_TEST_2 = new BSTree();
    public static final BSTree ADD_TREE_TEST_1 = new BSTree();
    public static final BSTree ADD_TREE_TEST_2 = new BSTree();
    public static final BSTree DELETE_TREE_TEST_1 = new BSTree();
    public static final BSTree DELETE_TREE_TEST_2 = new BSTree();
    public static final BSTree REVERSE_TREE_TEST_1 = new BSTree();
    public static final BSTree REVERSE_TREE_TEST_2 = new BSTree();

    public static final Node ROOT1;
    public static final Node ROOT2;
    public static final Node ROOT_ADD1;
    public static final Node ROOT_ADD2;
    public static final Node ROOT_DELETE1;
    public static final Node ROOT_DELETE2;
    public static final Node ROOT_REVERSE1;
    public static final Node ROOT_REVERSE2;

    static {
        Node node1_100 = new Node(100);
        Node node1_50 = new Node(50);
        Node node1_150 = new Node(150);
        Node node1_30 = new Node(30);
        Node node1_200 = new Node(200);
        Node node1_40 = new Node(40);
        Node node1_175 = new Node(175);
        Node node1_250 = new Node(250);

        node1_100.left = node1_50;
        node1_100.right = node1_150;
        node1_50.left = node1_30;
        node1_30.right = node1_40;
        node1_150.right = node1_200;
        node1_200.left = node1_175;
        node1_200.right = node1_250;

        ROOT2 = node1_100;
        ROOT1 = null;

        try {
            Field field1 = ORIGINAL_TREE_TEST_1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(ORIGINAL_TREE_TEST_1, ROOT1);

            Field field1_1 = ORIGINAL_TREE_TEST_1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(ORIGINAL_TREE_TEST_1, 0);

            Field field2 = ORIGINAL_TREE_TEST_2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(ORIGINAL_TREE_TEST_2, ROOT2);

            Field field2_1 = ADD_TREE_TEST_2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(ORIGINAL_TREE_TEST_2, 8);
        } catch (Exception e) {

        }
    }

    static {
        // add
        Node node1_100 = new Node(100);
        Node node1_50 = new Node(50);
        Node node1_150 = new Node(150);
        Node node1_30 = new Node(30);
        Node node1_200 = new Node(200);
        Node node1_40 = new Node(40);
        Node node1_175 = new Node(175);
        Node node1_250 = new Node(250);
        Node node1_55 = new Node(55);

        node1_100.left = node1_50;
        node1_100.right = node1_150;
        node1_50.left = node1_30;
        node1_30.right = node1_40;
        node1_150.right = node1_200;
        node1_200.left = node1_175;
        node1_200.right = node1_250;
        node1_50.right = node1_55;

        ROOT_ADD2 = node1_100;

        Node node1_47 = new Node(47);

        ROOT_ADD1 = node1_47;

        try {
            Field field1 = ADD_TREE_TEST_1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(ADD_TREE_TEST_1, ROOT_ADD1);

            Field field1_1 = ADD_TREE_TEST_1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(ADD_TREE_TEST_1, 1);

            Field field2 = ADD_TREE_TEST_2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(ADD_TREE_TEST_2, ROOT_ADD2);

            Field field2_1 = ADD_TREE_TEST_2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(ADD_TREE_TEST_2, 9);
        } catch (Exception e) {

        }
    }

    static {
        // delete
        Node node1_100 = new Node(100);
        Node node1_50 = new Node(50);
        Node node1_150 = new Node(150);
        Node node1_200 = new Node(200);
        Node node1_40 = new Node(40);
        Node node1_175 = new Node(175);
        Node node1_250 = new Node(250);

        node1_100.left = node1_50;
        node1_100.right = node1_150;
        node1_50.left = node1_40;
        node1_150.right = node1_200;
        node1_200.left = node1_175;
        node1_200.right = node1_250;

        Node node2_100 = new Node(100);
        Node node2_50 = new Node(50);
        Node node2_30 = new Node(30);
        Node node2_200 = new Node(200);
        Node node2_40 = new Node(40);
        Node node2_175 = new Node(175);
        Node node2_250 = new Node(250);

        node2_100.left = node2_50;
        node2_100.right = node2_200;
        node2_50.left = node2_30;
        node2_30.right = node2_40;
        node2_200.left = node2_175;
        node2_200.right = node2_250;

        ROOT_DELETE1 = node1_100;
        ROOT_DELETE2 = node2_100;

        try {
            Field field1 = DELETE_TREE_TEST_1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(DELETE_TREE_TEST_1, ROOT_DELETE1);

            Field field1_1 = DELETE_TREE_TEST_1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(DELETE_TREE_TEST_1, 7);

            Field field2 = DELETE_TREE_TEST_2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(DELETE_TREE_TEST_2, ROOT_DELETE2);

            Field field2_1 = DELETE_TREE_TEST_2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(DELETE_TREE_TEST_2, 7);
        } catch (Exception e) {

        }
    }

    static {
        // reverse
        Node node1_100 = new Node(100);
        Node node1_50 = new Node(50);
        Node node1_150 = new Node(150);
        Node node1_30 = new Node(30);
        Node node1_200 = new Node(200);
        Node node1_40 = new Node(40);
        Node node1_175 = new Node(175);
        Node node1_250 = new Node(250);

        node1_100.right = node1_50;
        node1_100.left = node1_150;
        node1_50.right = node1_30;
        node1_30.left = node1_40;
        node1_150.left = node1_200;
        node1_200.right = node1_175;
        node1_200.left = node1_250;

        ROOT_REVERSE2 = node1_100;
        ROOT_REVERSE1 = null;

        try {
            Field field1 = REVERSE_TREE_TEST_1.getClass().getDeclaredField("root");
            field1.setAccessible(true);
            field1.set(REVERSE_TREE_TEST_1, ROOT_REVERSE1);

            Field field1_1 = REVERSE_TREE_TEST_1.getClass().getDeclaredField("size");
            field1_1.setAccessible(true);
            field1_1.set(REVERSE_TREE_TEST_1, 0);

            Field field2 = REVERSE_TREE_TEST_2.getClass().getDeclaredField("root");
            field2.setAccessible(true);
            field2.set(REVERSE_TREE_TEST_2, ROOT_REVERSE2);

            Field field2_1 = REVERSE_TREE_TEST_2.getClass().getDeclaredField("size");
            field2_1.setAccessible(true);
            field2_1.set(REVERSE_TREE_TEST_2, 8);
        } catch (Exception e) {

        }
    }
}
